# Todo spec

## Initial page
* Go to todo app page
* Page contains "YOU MOFOCKOON"

## Add new todo

* Go to todo app page
* Add todo item "Go to Istanbul Coders event ~(^-^)~"
* Todo list contains "Go to Istanbul Coders event ~(^-^)~"

## Add new todo and complete

* Go to todo app page
* Add todo item "Prepare to Istanbul Coders event (Not probably..) ¯\\_( ͡° ͜ʖ ͡°)_/¯"
* Add todo item "Go to Istanbul Coders event ~(^-^)~"
* Add todo item "Eat some pizza! (づ｡◕‿‿◕｡)づ"
* Add todo item "Learn cool stuff! (⌐■_■)"
* Click on todo "Go to Istanbul Coders event ~(^-^)~"
* Todo "Go to Istanbul Coders event ~(^-^)~" should be completed
* Click on todo "Learn cool stuff! (⌐■_■)"
* Todo "Learn cool stuff! (⌐■_■)" should be completed
* Wait "1" seconds
