/* globals gauge*/
"use strict";
const { openBrowser, closeBrowser, write, goto, press, text, focus, textBox, click, button, toRightOf, $ } = require('taiko');
const headless = process.env.headless_chrome.toLowerCase() === 'true';
const assert = require("chai").assert

const timeout = ms => new Promise(res => setTimeout(res, ms))


beforeSuite(async () => {
    await openBrowser({ headless: true, args: ['--no-sandbox', '--disable-setuid-sandbox'] })
});

afterSuite(async () => {
    await timeout(3000)
    await closeBrowser();
});

step("Go to todo app page", async () => {
    await goto('http://localhost:3000');
});

step("Add todo item <todoItem>", async (todoItem) => {
    await focus(textBox())
    await write(todoItem);
    await press("Enter");
});

step("Click on todo <todoItem>", async (todoItem) => {
    await click(text(todoItem))
})

step("Todo <todoItem> should be completed", async (todoItem) => {
    const completedTodos = await $("li.completed label").text();
    assert.include(completedTodos, todoItem);
})

step("Page contains <content>", async (content) => {
    assert.isTrue(await text(content).exists());
});

step("Todo list contains <expectedTodo>", async (expectedTodo) => {
    assert.isTrue(await text(expectedTodo).exists());

});

step("Todo list not contains <expectedTodo>", async (expectedTodo) => {
    assert.isFalse(await text(expectedTodo).exists());
});

step("Wait <s> seconds", async (seconds) => {
    await timeout(parseInt(seconds) * 1000)
})
