const { openBrowser, goto, write, press, checkBox, closeBrowser } = require('taiko');
(async () => {
    try {
        await openBrowser();
        await goto("35.223.155.49:3000");
        await write("taiko test automation");
        await press("Enter");
        await checkBox("Learn Javascript").check();
        await checkBox("Learn Javascript").uncheck();
        await checkBox("Learn Javascript").check();
    } catch (error) {
        console.error(error);
    } finally {
        await closeBrowser();
    }
})();
